-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 11 nov. 2021 à 10:45
-- Version du serveur : 10.4.20-MariaDB
-- Version de PHP : 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `suivibudgetaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `tannee_comptable`
--

CREATE TABLE `tannee_comptable` (
  `id_annee_compt` int(11) NOT NULL,
  `annee` varchar(127) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tannee_comptable`
--

INSERT INTO `tannee_comptable` (`id_annee_compt`, `annee`, `date_creation`, `date_modification`, `date_suppression`) VALUES
(1, '2021', '2021-11-08 21:53:13', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tassociation_cat_eglise`
--

CREATE TABLE `tassociation_cat_eglise` (
  `id_association` int(11) NOT NULL,
  `id_eglise` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `date_changement` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tassociation_cat_eglise`
--

INSERT INTO `tassociation_cat_eglise` (`id_association`, `id_eglise`, `id_categorie`, `date_changement`, `date_creation`, `date_modification`, `date_suppression`) VALUES
(1, 3, 1, '2021-11-08 22:03:43', '2021-11-08 14:03:43', '2021-11-08 14:03:43', NULL),
(2, 3, 2, '2021-11-08 22:26:18', '2021-11-08 14:26:18', '2021-11-08 14:26:18', NULL),
(3, 3, 1, '2021-11-08 22:28:49', '2021-11-08 14:28:49', '2021-11-08 14:28:49', NULL),
(4, 3, 1, '2021-11-08 22:29:04', '2021-11-08 14:29:04', '2021-11-08 14:29:04', NULL),
(5, 3, 2, '2021-11-08 22:29:22', '2021-11-08 14:29:22', '2021-11-08 14:29:22', NULL),
(6, 3, 1, '2021-11-10 22:11:22', '2021-11-08 14:33:04', '2021-11-08 14:33:04', NULL),
(7, 4, 2, '2021-11-08 22:53:54', '2021-11-08 14:53:54', '2021-11-08 14:53:54', NULL),
(8, 5, 1, '2021-11-11 00:41:14', '2021-11-10 16:41:14', '2021-11-10 16:41:14', NULL),
(9, 6, 1, '2021-11-11 00:41:34', '2021-11-10 16:41:34', '2021-11-10 16:41:34', NULL),
(10, 7, 2, '2021-11-11 09:08:47', '2021-11-11 01:08:47', '2021-11-11 01:08:47', NULL),
(11, 8, 2, '2021-11-11 09:09:44', '2021-11-11 01:09:44', '2021-11-11 01:09:44', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tcategorie`
--

CREATE TABLE `tcategorie` (
  `id_categorie` int(11) NOT NULL,
  `designation` varchar(127) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tcategorie`
--

INSERT INTO `tcategorie` (`id_categorie`, `designation`, `date_creation`, `date_modification`, `date_suppression`) VALUES
(1, '1e', '2021-11-08 21:49:09', NULL, NULL),
(2, '2e', '2021-11-08 21:49:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tdepense`
--

CREATE TABLE `tdepense` (
  `id_depense` int(11) NOT NULL,
  `montant` int(11) NOT NULL,
  `motif` varchar(127) NOT NULL,
  `date_depense` timestamp NULL DEFAULT current_timestamp(),
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tdepense`
--

INSERT INTO `tdepense` (`id_depense`, `montant`, `motif`, `date_depense`, `date_creation`, `date_modification`, `date_suppression`) VALUES
(1, 50000, 'Nous avons ...', NULL, '2021-11-10 13:18:04', '2021-11-10 13:18:53', '2021-11-10 13:18:53'),
(2, 50000, 's ipsum doloribus accusamu', NULL, '2021-11-10 13:20:14', '2021-11-10 13:31:21', '2021-11-10 13:31:21');

-- --------------------------------------------------------

--
-- Structure de la table `tdistrict`
--

CREATE TABLE `tdistrict` (
  `id_district` int(11) NOT NULL,
  `nom` varchar(127) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tdistrict`
--

INSERT INTO `tdistrict` (`id_district`, `nom`, `date_creation`, `date_modification`, `date_suppression`) VALUES
(1, 'Dieu est grand', '2021-11-08 08:37:20', '2021-11-08 12:01:14', '2021-11-08 12:01:14'),
(2, 'Kqn', '2021-11-08 08:42:28', '2021-11-08 12:01:25', '2021-11-08 12:01:25'),
(3, 'District 2', '2021-11-08 12:03:41', '2021-11-08 12:03:46', '2021-11-08 12:03:46'),
(4, 'District des districts', '2021-11-08 12:05:34', '2021-11-08 12:05:38', '2021-11-08 12:05:38'),
(5, 'ad', '2021-11-08 12:08:17', '2021-11-08 12:08:28', '2021-11-08 12:08:28'),
(6, 'adsfasd', '2021-11-08 12:08:22', '2021-11-08 12:10:40', '2021-11-08 12:10:40'),
(7, 'adsfasd', '2021-11-08 12:08:26', '2021-11-08 12:10:46', '2021-11-08 12:10:46'),
(8, 'ad', '2021-11-08 12:11:06', '2021-11-08 12:20:58', '2021-11-08 12:20:58'),
(9, 'God is supernatural ', '2021-11-08 12:21:05', '2021-11-08 12:43:12', '2021-11-08 12:43:12'),
(10, 'Yes he is', '2021-11-08 12:29:11', '2021-11-08 12:43:58', '2021-11-08 12:43:58'),
(11, 'Dieu Fidel', '2021-11-08 12:45:59', '2021-11-10 15:16:02', '2021-11-10 15:16:02'),
(12, 'GOMA', '2021-11-08 13:38:11', '2021-11-10 16:39:59', NULL),
(13, 'GOMA-EST', '2021-11-10 16:39:54', '2021-11-10 16:39:54', NULL),
(14, 'GOMA NORD', '2021-11-10 17:24:14', '2021-11-10 17:24:14', NULL),
(15, 'MASIZA', '2021-11-11 01:06:55', '2021-11-11 01:06:55', NULL),
(16, 'ZIRALO', '2021-11-11 01:07:28', '2021-11-11 01:07:28', NULL),
(17, 'SAKE', '2021-11-11 01:07:48', '2021-11-11 01:07:48', NULL),
(18, 'MUJA', '2021-11-11 01:09:08', '2021-11-11 01:09:08', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `teglise`
--

CREATE TABLE `teglise` (
  `id_eglise` int(11) NOT NULL,
  `nom` varchar(127) NOT NULL,
  `id_district` int(11) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppresion` timestamp NULL DEFAULT NULL,
  `nom_pasteur` varchar(127) NOT NULL,
  `telephone` varchar(127) NOT NULL,
  `commune` varchar(127) NOT NULL,
  `quartier` varchar(127) NOT NULL,
  `avenue` varchar(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `teglise`
--

INSERT INTO `teglise` (`id_eglise`, `nom`, `id_district`, `date_creation`, `date_modification`, `date_suppresion`, `nom_pasteur`, `telephone`, `commune`, `quartier`, `avenue`) VALUES
(1, 'Sinai', 11, '2021-11-08 13:00:43', '2021-11-08 14:18:40', '2021-11-08 14:18:40', 'Triphon', '+243975693666', 'Goma', 'Lamuka', 'Kambove'),
(2, 'Uzima mupya', 12, '2021-11-08 13:37:48', '2021-11-08 13:38:46', '2021-11-08 13:38:46', 'Moganla', '+243975695658', 'Kampemba', 'Bel Air', 'DE plaine coin Laurier'),
(3, 'PP UNILU 2', 13, '2021-11-08 14:03:43', '2021-11-10 16:40:55', NULL, 'Kalombo MK', '+24397695895', 'Lubumbashi', 'Gambela II', 'Route Kasapa'),
(4, 'La grace', 12, '2021-11-08 14:53:54', '2021-11-10 15:17:54', NULL, 'Didier', '+243', 'Bel air', 'Rw', 's'),
(5, 'Dieu vuvant', 12, '2021-11-10 16:41:14', '2021-11-10 16:41:14', NULL, 'ads', 'ads', 'ad', 'ad', 'asd'),
(6, 'Assemble des saints', 12, '2021-11-10 16:41:34', '2021-11-10 16:41:34', NULL, 'ads', 'ad', 'df', 'afd', 'af'),
(7, 'MIZEITUNI / SAKE', 17, '2021-11-11 01:08:47', '2021-11-11 01:08:47', NULL, 'Mungala', '+243975693666', 'sake', '1e', '1e'),
(8, 'FARAJA', 18, '2021-11-11 01:09:44', '2021-11-11 01:09:44', NULL, 'joel', '+243', 'Muja', 'e', 'e');

-- --------------------------------------------------------

--
-- Structure de la table `tmontant_annee`
--

CREATE TABLE `tmontant_annee` (
  `id_montant_annee` int(11) NOT NULL,
  `montant` int(11) NOT NULL,
  `id_annee_compt` int(11) NOT NULL,
  `id_categ` int(11) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tmontant_annee`
--

INSERT INTO `tmontant_annee` (`id_montant_annee`, `montant`, `id_annee_compt`, `id_categ`, `date_creation`, `date_modification`, `date_suppression`) VALUES
(1, 5, 1, 1, '2021-11-08 21:54:49', NULL, NULL),
(2, 10, 1, 2, '2021-11-08 21:54:49', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `trecette`
--

CREATE TABLE `trecette` (
  `id_recette` int(11) NOT NULL,
  `montant` int(11) NOT NULL,
  `id_eglise` int(11) NOT NULL,
  `mois_reference` varchar(127) NOT NULL,
  `date_recette` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `trecette`
--

INSERT INTO `trecette` (`id_recette`, `montant`, `id_eglise`, `mois_reference`, `date_recette`, `date_creation`, `date_modification`, `date_suppression`) VALUES
(1, 10, 4, '1', '2021-11-11 09:26:16', '2021-11-11 01:26:16', '2021-11-11 01:26:16', NULL),
(2, 4, 4, '2', '2021-11-11 09:26:16', '2021-11-11 01:26:16', '2021-11-11 01:26:16', NULL),
(3, 5, 3, '1', '2021-11-11 09:26:26', '2021-11-11 01:26:26', '2021-11-11 01:26:26', NULL),
(4, 1, 3, '2', '2021-11-11 09:26:26', '2021-11-11 01:26:26', '2021-11-11 01:26:26', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tutilisateur`
--

CREATE TABLE `tutilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `nom_utilisateur` varchar(127) NOT NULL,
  `mot_de_passe` varchar(127) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppresion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tutilisateur`
--

INSERT INTO `tutilisateur` (`id_utilisateur`, `nom_utilisateur`, `mot_de_passe`, `date_creation`, `date_modification`, `date_suppresion`) VALUES
(1, 'baraka', '159a33dc61da7305c5b70f55c522fcb3e81b1704', '2021-11-08 06:14:44', '2021-11-08 06:14:44', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `tannee_comptable`
--
ALTER TABLE `tannee_comptable`
  ADD PRIMARY KEY (`id_annee_compt`);

--
-- Index pour la table `tassociation_cat_eglise`
--
ALTER TABLE `tassociation_cat_eglise`
  ADD PRIMARY KEY (`id_association`);

--
-- Index pour la table `tcategorie`
--
ALTER TABLE `tcategorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Index pour la table `tdepense`
--
ALTER TABLE `tdepense`
  ADD PRIMARY KEY (`id_depense`);

--
-- Index pour la table `tdistrict`
--
ALTER TABLE `tdistrict`
  ADD PRIMARY KEY (`id_district`);

--
-- Index pour la table `teglise`
--
ALTER TABLE `teglise`
  ADD PRIMARY KEY (`id_eglise`),
  ADD KEY `fk_district` (`id_district`);

--
-- Index pour la table `tmontant_annee`
--
ALTER TABLE `tmontant_annee`
  ADD PRIMARY KEY (`id_montant_annee`),
  ADD KEY `fk_categorie` (`id_categ`),
  ADD KEY `fk_annee_comptable` (`id_annee_compt`);

--
-- Index pour la table `trecette`
--
ALTER TABLE `trecette`
  ADD PRIMARY KEY (`id_recette`),
  ADD KEY `fk_eglise` (`id_eglise`);

--
-- Index pour la table `tutilisateur`
--
ALTER TABLE `tutilisateur`
  ADD PRIMARY KEY (`id_utilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `tassociation_cat_eglise`
--
ALTER TABLE `tassociation_cat_eglise`
  MODIFY `id_association` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `tcategorie`
--
ALTER TABLE `tcategorie`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tdepense`
--
ALTER TABLE `tdepense`
  MODIFY `id_depense` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tdistrict`
--
ALTER TABLE `tdistrict`
  MODIFY `id_district` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `teglise`
--
ALTER TABLE `teglise`
  MODIFY `id_eglise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `tmontant_annee`
--
ALTER TABLE `tmontant_annee`
  MODIFY `id_montant_annee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `trecette`
--
ALTER TABLE `trecette`
  MODIFY `id_recette` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tutilisateur`
--
ALTER TABLE `tutilisateur`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `teglise`
--
ALTER TABLE `teglise`
  ADD CONSTRAINT `fk_district` FOREIGN KEY (`id_district`) REFERENCES `tdistrict` (`id_district`);

--
-- Contraintes pour la table `tmontant_annee`
--
ALTER TABLE `tmontant_annee`
  ADD CONSTRAINT `fk_annee_comptable` FOREIGN KEY (`id_annee_compt`) REFERENCES `tannee_comptable` (`id_annee_compt`),
  ADD CONSTRAINT `fk_categorie` FOREIGN KEY (`id_categ`) REFERENCES `tcategorie` (`id_categorie`);

--
-- Contraintes pour la table `trecette`
--
ALTER TABLE `trecette`
  ADD CONSTRAINT `fk_eglise` FOREIGN KEY (`id_eglise`) REFERENCES `teglise` (`id_eglise`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="<?= base_url() ?>/assets/fonts/icomoon/style.css">

  <link rel="stylesheet" href="<?= base_url() ?>/assets/css/owl.carousel.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/css/bootstrap.min.css">

  <!-- Style -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/css/style.css">

  <title>Authentificaition</title>
</head>

<body>


  <div class="d-md-flex half">
    <div class="bg" style="background-image: url('<?= base_url() ?>/assets/images/bg_1.jpg');"></div>
    <div class="contents">

      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-12">
            <div class="form-block mx-auto">
              <div class="text-center mb-5">

                <h4 class="text-uppercase">Application de <strong>Suivi-Budgetaire</strong></h4>
                <h5>- Authentificaition - </h5>
              </div>
              <form action="<?= base_url("Home/se_connecter") ?>" method="post">
                <div class="form-group first">
                  <label for="username">Nom d'utilisateur</label>
                  <input type="text" required class="form-control" placeholder="" name="nom_utilisateur" id="nom_utilisateur">
                </div>
                <div class="form-group last mb-3">
                  <label for="password">Mot de passe</label>
                  <input type="password" required class="form-control" placeholder="*****************" name="mot_de_passe" id="mot_de_passe">
                </div>
                <?php
                if (session()->getFlashdata('echec_connexion')) {
                ?>
                  <p>
                    <center style="color: red;">
                      <?php
                      echo session()->getFlashdata('echec_connexion');
                      ?>
                      <br>
                    </center>
                  </p>
                <?php
                }
                ?>
                <input type="submit" value="Se connecter" class="btn btn-block py-2 btn-primary">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>



  <script hrc="<?= base_url() ?>/assets/js/jquery-3.3.1.min.js"></script>
  <script hrc="<?= base_url() ?>/assets/js/popper.min.js"></script>
  <script hrc="<?= base_url() ?>/assets/js/bootstrap.min.js"></script>
  <script hrc="<?= base_url() ?>/assets/js/main.js"></script>
</body>

</html>
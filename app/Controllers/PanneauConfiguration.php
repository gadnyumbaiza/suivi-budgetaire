<?php
//
namespace App\Controllers;

use App\Models\AnneeComptableModel;
use App\Models\AssociationCatEgliseModel;
use App\Models\CategorieModel;
use App\Models\DepenseModel;
use App\Models\UtilisateurModel;
use App\Models\DistrictModel;
use App\Models\EgliseModel;
use App\Models\MontantAnneeModel;
use App\Models\RecetteModel;
use App\ThirdParty\PDF;

//
/**
 * PanneauConfiguration
 */


class PanneauConfiguration extends BaseController
{

    private $egliseModel;
    private $depenseModel;
    private $districtModel;
    private $utilsiteurModel;
    private $anneComptableModel;
    private $asssociationCatEgliseModel;
    private $recetteModel;
    private $categorieModel;
    private $montantAnnee;


    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->egliseModel = new EgliseModel();
        $this->districtModel = new DistrictModel();
        $this->depenseModel = new DepenseModel();
        $this->recetteModel = new RecetteModel();
        $this->asssociationCatEgliseModel = new AssociationCatEgliseModel();
        $this->utilsiteurModel = new UtilisateurModel();
        $this->anneComptableModel = new AnneeComptableModel();
        $this->categorieModel = new CategorieModel();
        $this->montantAnnee = new MontantAnneeModel();
    }
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $data = [
            'nombres' => $this->get_nombres(),
            'version'          => '1.0.0',
            'nomUtilisateur' => session()->get('nom_utilisateur'),
        ];
        return view('index', $data);
    }

    /**
     * get_nombres
     *
     * @return void
     */
    private function get_nombres()
    {

        $districts = new DistrictModel();
        $eglises = new EgliseModel();
        $nombres = [
            'nombreEglsie' => count($eglises->findAll()),
            'nombreDisctrict' => count($districts->findAll())
        ];

        return $nombres;
    }

    /**
     * get_infos_app
     *
     * @return void
     */
    private function get_infos_app()
    {
        $data['version'] = '1.0.0';
        $data['nom_utilisateur']  = session()->get('nom_utilisateur');

        return $data;
    }

    /**
     * retrieve_appropriete_model
     *
     * @param  mixed $value
     * @return mixte
     */
    private function retrieve_appropriete_model($value)
    {
        switch ($value) {
            case 'eglise':
                return $this->egliseModel;
                break;
            case 'district':
                return  $this->districtModel;
                break;
            case 'depense':
                return $this->depenseModel;
                break;
            case 'anneecomptable':
                return $this->anneComptableModel;
                break;
            case 'recette':
                return $this->recetteModel;
                break;
            case 'categorie':
                return $this->categorieModel;
                break;
            default:
                return null;
                break;
        }
        return null;
    }

    /**
     * nv_mois_reference
     *
     * @param  mixed $id_eglise
     * @return void
     */
    public function nv_mois_reference($id_eglise)
    {
        $mois = 1;
        $dernière_contribution = $this->recetteModel->where('id_eglise', $id_eglise)->orderBy("date_recette", "DESC")->first();
        if ($dernière_contribution != null) {
            $mois =  $dernière_contribution['mois_reference'];

            if ($mois != 12)
                $mois += 1;
            else
                $mois = -1;
        }
        return $mois;
    }

    /**
     * get_montant_by_eglise
     *
     * @param  mixed $id_eglise
     * @return void
     */
    public function get_montant_by_eglise($id_eglise)
    {
        $acem = $this->asssociationCatEgliseModel->where(['id_eglise' => $id_eglise])->orderBy("id_eglise", "DESC")->first();
        $mac = $this->montantAnnee->where(['id_categ' => $acem['id_categorie']])->orderBy("id_categ", "DESC")->first();
        return $mac['montant'];
    }



    //  **********************************************  C  R  U   D *********************************


    // A J O U T

    /**
     * ajout
     *
     * @param  mixed $onglet
     * @return void
     */
    public function ajout($onglet)
    {
        $session = session();
        $model = $this->retrieve_appropriete_model($onglet);

        switch ($onglet) {
            case 'eglise':
                $champs = ['nom', 'id_district', 'nom_pasteur', 'telephone', 'commune', 'quartier', 'avenue'];
                $champs2 = ['id_categorie'];
                break;
            case 'district':
                $champs = ['nom'];
                break;
            case 'depense':
                $champs = ['montant', 'motif', 'date_depense'];
                break;
            case 'anneecomptable':
                $champs = ['annee'];
                break;
            case 'recette':
                $champs = ['montant', 'id_eglise', 'mois_reference', 'date_recette'];
                break;
            case 'categorie':
                $champs = ['designation'];
                break;
            default:
                return  redirect()->to(site_url('/PanneauConfiguration/'));
                break;
        }

        $data =  $this->recuperation_donnees($champs);
        if ($model != null) {
            if ($onglet == "recette") {

                $montant_par_categorie = $this->asssociationCatEgliseModel;
                $data['mois_reference'] = $this->nv_mois_reference($data['id_eglise']);
                if ($montant_attendu = $this->get_montant_by_eglise($data['id_eglise'])) {
                    if ($data['montant'] > $montant_attendu) {

                        $qtef =  $data['montant'] / $montant_attendu;
                        $qte =  intdiv($data['montant'], $montant_attendu);
                        $temps_montant =  $data['montant'] - ($qte *  $montant_attendu);
                        $data['montant'] = $montant_attendu;

                        for ($i = 1; $i <= $qte; $i++) {
                            if ($qte ==  $qtef) {
                                if ($i == $qte && $montant_attendu == $data['montant']) {
                                    continue;
                                }
                            }

                            $id = $model->insert($data);
                            $data['mois_reference'] += 1;
                            $montant_attendu -= $data['montant'];
                        }

                        $data['montant'] =  $temps_montant;
                    }
                } else {
                }
            }
            if ($onglet == "depense") {
                $total_recette = $this->toutes_contribution();
                $total_depenses =$this->toutes_depenses();

                $reste = $total_recette - $total_depenses;
                if ($data['montant'] > $reste) {
                    $session->setFlashdata('echec_ajout_depense', "Les recettes ($reste $) sont insuffisantes !");
                    
                    return  redirect()->to(site_url('/PanneauConfiguration/affichage/depense/ajout'));
                    exit;
                }
            }
            if ($id = $model->insert($data)) {
                if ($onglet == "eglise") {
                    $data2 =  $this->recuperation_donnees($champs2);
                    $data2['id_eglise'] = $id;

                    if ($id2 = $this->asssociationCatEgliseModel->insert($data2)) {
                    }
                }
                $session->setFlashdata('ajout_success', "L'ajout s'est effectué avec success !");
                return  redirect()->to(site_url('/PanneauConfiguration/affichage/' . $onglet));
            } else {
                $session->setFlashdata('echec_ajout', "Echec d'inserton !");
                return  redirect()->to(site_url('/PanneauConfiguration/affichage/' . $onglet . '/ajout/'));
            }
        }
    }
    /**
     * recuperation_donnees
     *
     * @param  mixed $champs
     * @return void
     */
    public function recuperation_donnees($champs)
    {
        $data = [];
        foreach ($champs as $key => $value) {
            $data[$value] = $this->request->getVar($value);
        }
        return $data;
    }
    // A F F I C H A G E

    /**
     * affichage
     *
     * @param  mixed $onglet
     * @param  mixed $action
     * @param  mixed $value_param
     * @return void
     */
    public function affichage($onglet = "", $action = "", $value_param = "")
    {
        //
        $infospage = $this->get_infos_app();
        $infospage['onglet'] = $onglet;
        $onglet = strtolower($onglet);
        $link = $onglet;
        $action = "/" . $action;

        //
        switch ($onglet) {
                // case "profile":
                //     var_dump(session()->get("nom_utilisateur"));
                //     $infospage['utilisateur'] = $this->utilsiteurModel->where(['nom_utilisateur', session()->get("nom_utilisateur")])->first();

                //     var_dump($infospage['utilisateur']);
                //     exit;
                //     break;

            case "eglise":
                $eglises = $this->egliseModel->findAll();
                for ($i = 0; $i < count($eglises); $i++) {
                    $eglises[$i]['id_district'] = $this->districtModel->where(['id_district' => $eglises[$i]['id_district']])->first();
                }
                $infospage['eglises'] = $eglises;
                $infospage['districts'] = $this->districtModel->findAll();
                $infospage['categories'] = $this->categorieModel->findAll();
                break;
            case "district":
                $infospage['districts'] = $this->districtModel->findAll();
                break;
            case "recette":
                $recettes =   $this->recetteModel->findAll();
                for ($i = 0; $i < count($recettes); $i++) {
                    $recettes[$i]['id_eglise'] = $this->egliseModel->where(['id_eglise' => $recettes[$i]['id_eglise']])->first();
                }
                $infospage['recettes'] =   $recettes;
                $infospage['eglises'] =   $this->egliseModel->findAll();
                $infospage['districts'] = $this->districtModel->findAll();
                break;
            case 'depense':
                $infospage['depenses'] = $this->depenseModel->findAll();
                break;
            default:
                return redirect()->to(site_url('/PanneauConfiguration/index'));
                break;
        }
        if (!empty($action)) {

            if ($action != "/ajout" && $action != "/modification") {
                $action = "/liste";
            }
            if ($action == "/modification") {
                if (!empty($value_param)) {
                    $infospage[$onglet] = $this->retrieve_appropriete_model($onglet)->where(['id_' . $onglet => $value_param])->first();
                    if ($onglet == "eglise") {
                        $infospage["id_categorie"] = $this->asssociationCatEgliseModel->where('id_eglise', $value_param)->orderBy("date_changement", "DESC")->first();
                    }

                    if ($onglet == "recette") {
                        $infospage["id_eglise"] = $this->egliseModel->where('id_eglise', $infospage[$onglet]['id_eglise'])->first();
                    }
                }
            }
        } else {
            $action = "/liste";
        }
        $link .= $action;
        //
        return view($link,  $infospage);
    }
    // S U P P R E S S I O N    
    /**
     * suppression
     *
     * @param  mixed $onglet
     * @param  mixed $id
     * @return void
     */
    public function suppression($onglet, $id)
    {
        $session = session();
        if (!empty($id)) {
            if ($this->retrieve_appropriete_model($onglet)->delete($id)) {
                $session->setFlashdata('suppression_success', "La suppression s'est effectué avec success !");
                return redirect()->to(site_url('/PanneauConfiguration/affichage/' . $onglet));
            }
        }
    }
    // M O D I F I C A T I O N


    /**
     * modification
     *
     * @param  mixed $onglet
     * @return void
     */
    public function modification($onglet)
    {

        switch ($onglet) {
            case 'eglise':
                $champs = ['nom', 'id_district', 'nom_pasteur', 'telephone', 'commune', 'quartier', 'avenue'];
                $champs2 = ['id_categorie'];
                break;
            case 'district':
                $champs = ['nom'];
                break;
            case 'depense':
                $champs = ['montant', 'motif', 'date_depense'];
                break;
            case 'anneecomptable':
                $champs = ['annee'];
                break;
            case 'recette':
                $champs = ['montant', 'id_eglise', 'mois_reference', 'date_recette'];
                break;
            case 'categorie':
                $champs = ['designation'];
                break;
            default:
                return  redirect()->to(site_url('/PanneauConfiguration/'));
                break;
        }
        $data = $this->recuperation_donnees($champs);
        $id = $this->recuperation_donnees(['id_' . $onglet]);

        $session = session();

        if (!empty($id)) {
            if ($this->retrieve_appropriete_model($onglet)->update($id, $data)) {
                if ($onglet == "eglise") {
                    $data2 =  $this->recuperation_donnees($champs2);
                    $data2['id_eglise'] = $id['id_eglise'];
                    $last_id_categorie = $this->recuperation_donnees(['last_id_categorie']);

                    if ($last_id_categorie['last_id_categorie'] != $data2['id_categorie']) {
                        if ($id2 = $this->asssociationCatEgliseModel->insert($data2)) {
                        }
                    }
                }

                $session->setFlashdata('modification_success', "La modification s'est effectuée avec success !");
                return redirect()->to(site_url('/PanneauConfiguration/affichage/' . $onglet));
            } else {
                $session->setFlashdata('echec_ajout', "Echec de modification !");
                return  redirect()->to(site_url('/PanneauConfiguration/affichage/' . $onglet . '/modification/' . $id));
            }
        }
    }


    //******************** I M P R E S S I O N S *****************************/

    public function impression($entite)
    {
        switch ($entite) {
            case "recette":
                $this->print_recette();
                break;
            case "depense":
                //do somethng 
                break;
            default:
                //do something
                break;
        }
    }

    public function print_recette()
    {
        $pdf = new PDF();
        $pdf->AddPage();
        $pdf->SetFont('times', '', 11);
        $pdf->Ln(6);
        $pdf->SetX(10);

        $pdf->Ln(6);
        $pdf->SetX(60);

        $mois_annee = date("m / Y");
        $pdf->Cell(80, 6, utf8_decode("ANNEE 2021 JUSQU'AU MOIS DE " . $mois_annee), 0, 0, 'C', 0);
        $pdf->Ln(6);
        $pdf->SetX(60);
        $pdf->Cell(80, 6, utf8_decode("CONTRIBUTION DES EGLISES AU BUDGET COMMUNAUTAIRE"), 0, 0, 'C', 0);
        $pdf->Ln(6);
        $pdf->SetX(60);
        $date = date('d/m/Y');
        $pdf->Cell(80, 6, utf8_decode("DATE CONSIDEREE " . $date), 0, 0, 'C', 0);
        $pdf->Line(20, 60, 210 - 20, 60); // 20mm from each edge

        $pdf->Ln(20);
        $pdf->SetFont('times', '', 12);
        $debut = 10;
        $longeur = 10;
        $pdf->SetX($debut);
        $pdf->Cell($longeur, 12, utf8_decode("#"), 1, 0, 'C', 0);
        $pdf->SetX($pdf->GetX());
        $pdf->Cell($longeur * 5, 12, utf8_decode("District"), 1, 0, 'C', 0);
        $pdf->SetX($pdf->GetX());
        $pos_eglise = $pdf->GetX();
        $pdf->Cell($longeur * 7, 12, utf8_decode("Eglige"), 1, 0, 'C', 0);
        $pdf->SetX($pdf->GetX());
        $pos_paye = $pdf->GetX();
        $pdf->Cell($longeur * 6, 6, utf8_decode("2021"), 1, 0, 'C', 0);
        $pdf->Ln(6);
        $pdf->SetX($pos_paye);
        $pdf->Cell($longeur * 3, 6, utf8_decode("PAYE"), 1, 0, 'C', 0);
        $pdf->SetX($pdf->GetX());
        $pdf->Cell($longeur * 3, 6, utf8_decode("DETE"), 1, 0, 'C', 0);

        $districts = $this->districtModel->findAll();

        $compte_eglises = 0;
        $total_general_dette = 0;
        $total_general_percu = 0;

        foreach ($districts as $key => $district) {

            //
            $pdf->SetFont('times', '', 12);
            $eglises = $this->egliseModel->where(['id_district' => $district['id_district']])->findAll();
            $total_eglise = count($eglises) > 0 ?  count($eglises) : 1;
            $pdf->Ln(6);
            $pdf->SetX($debut);
            $pdf->Cell($longeur, 6 * ($total_eglise +  1), utf8_decode($key + 1), 1, 0, 'L', 0);
            $pdf->SetX($pdf->GetX());
            $pdf->Cell($longeur * 5, 6 * ($total_eglise + 1), utf8_decode($district['nom']), 1, 0, 'L', 0);

            //
            $total_percu = 0;
            $total_dette = 0;

            //
            if (count($eglises) > 0) {

                foreach ($eglises as $k => $eglise) {
                    $compte_eglises++;
                    $border = "LR";

                    if ($k == 0) {
                        $border = "TLR";
                    }
                    $pdf->SetX($pos_eglise);
                    $pdf->Cell($longeur * 7, 6, utf8_decode(($k + 1) . '. ' . $eglise['nom']), $border, 0, 'L', 0);
                    $pdf->SetX($pdf->GetX());
                    //
                    $result = $this->calcul_contribution_by_eglise($eglise['id_eglise']);
                    $total_dette += $result['dette'];
                    $total_percu += $result['paye'];
                    //
                    $pdf->Cell($longeur * 3, 6, utf8_decode($result['paye']), $border, 0, 'C', 0);
                    $pdf->SetX($pdf->GetX());
                    $pdf->Cell($longeur * 3, 6, utf8_decode($result['dette']), $border, 0, 'C', 0);

                    // if ($k < count($eglises) - 1) {
                    $pdf->Ln(6);
                    // }
                }
            } else {
                $pdf->SetX($pdf->GetX());
                $pdf->Cell($longeur * 7, 6, utf8_decode("-"), 1, 0, 'C', 0);
                $pdf->SetX($pdf->GetX());
                $pdf->Cell($longeur * 3, 6, utf8_decode("-"), 1, 0, 'C', 0);
                $pdf->SetX($pdf->GetX());
                $pdf->Cell($longeur * 3, 6, utf8_decode("-"), 1, 0, 'C', 0);
                $pdf->Ln(6);
            }

            //
            $pdf->SetFont('times', 'I', 12);

            $pdf->SetFillColor(232, 230, 223);



            $border = 1;
            $pdf->SetX($pos_eglise);
            $pdf->Cell($longeur * 7, 6, utf8_decode("TOTAL"), $border, 0, 'R', 1);
            $pdf->SetX($pdf->GetX());
            $pdf->Cell($longeur * 3, 6, utf8_decode($total_percu), $border, 0, 'C', 1);
            $pdf->SetX($pdf->GetX());
            $pdf->Cell($longeur * 3, 6, utf8_decode($total_dette), $border, 0, 'C', 1);

            //
            $total_general_dette += $total_dette;
            $total_general_percu += $total_percu;
        }

        $pdf->SetFillColor(189, 187, 183);
        $pdf->SetFont('times', 'I', 12);
        $pdf->Ln(6);
        $pdf->SetX($debut);
        $pdf->Cell($longeur + $longeur * 5, 6, utf8_decode("TOTAL GENERAL"), 1, 0, 'C', 1);
        $pdf->SetX($pdf->GetX());
        $pdf->Cell($longeur * 7, 6, utf8_decode($compte_eglises . " Egliges"), 1, 0, 'C', 1);
        $pdf->SetX($pdf->GetX());
        $pdf->Cell($longeur * 3, 6, utf8_decode($total_general_percu), 1, 0, 'C', 1);
        $pdf->SetX($pdf->GetX());
        $pdf->Cell($longeur * 3, 6, utf8_decode($total_general_dette), 1, 0, 'C', 1);

        $pdf->Output("contribution au 11 novembre" . $date . ".pdf", "D");
    }

    public function calcul_contribution_by_eglise($id_eglise)
    {
        $recette_eglise = $this->recetteModel->where(['id_eglise' => $id_eglise])->findAll();
        $montant_total = 0;
        $mois_actuelle  = date('m') - 1;
        foreach ($recette_eglise as $key => $recette) {
            $montant_total += $recette['montant'];
        }

        $montant_attendu = $this->get_montant_by_eglise($id_eglise) * $mois_actuelle;
        $dette = $montant_attendu - $montant_total;

        return ['paye' => $montant_total, 'dette' => $dette];
    }

    public function toutes_contribution()
    {
        $montant_total  = 0;

        $recette_eglise = $this->recetteModel->findAll();
        foreach ($recette_eglise as $key => $recette) {
            $montant_total += $recette['montant'];
        }
        return $montant_total;
    }

    public function toutes_depenses()
    {
        $montant_total  = 0;

        $depenses = $this->depenseModel->findAll();
        foreach ($depenses as $key => $depense) {
            $montant_total += $depense['montant'];
        }
        return $montant_total;
    }
}

<?php

namespace App\Controllers;

use App\Models\UtilisateurModel;

/**
 * Home
 */
class Home extends BaseController
{
	/**
	 * index
	 *
	 * @return void
	 */
	public function index()
	{
		if (!session()->get('nom_utilisateur')) {
			helper(['form', 'url']);
			echo view('auth/authentification');
		} else return redirect()->to(site_url('/PanneauConfiguration'));
	}

	/**
	 * se_connecter
	 *
	 * @return void
	 */
	public function se_connecter()
	{
		$this->utilisateurs = new UtilisateurModel();
		//Access  a la base des données

		helper(['form', 'url']);
		$session = session();

		$donnees = [
			'nom_utilisateur' => $this->request->getVar('nom_utilisateur'),
			'mot_de_passe'  => sha1($this->request->getVar('mot_de_passe')),
		];
		$connected = $this->utilisateurs->where($donnees)->first();
		if ($connected) {
			$session->set($connected);
			return redirect()->to(site_url('/PanneauConfiguration'));
		} else {
			$session->setFlashdata('echec_connexion', "Mot de passe ou nom d'utilisateur incorrect");
			return redirect()->to(site_url('/'));
		}
	}

	/**
	 * deconnexion
	 *
	 * @return void
	 */
	public function deconnexion()
	{
		session()->destroy();
		return redirect()->to(site_url('/'));
	}


	/**
	 * enregistrement_utilisateur
	 *
	 * @return void
	 */
	public function enregistrement_utilisateur()
	{

		$login = [];
		$this->utilisateur = new UtilisateurModel();

		$login = ['nom_utilisateur' => $this->request->getVar('nom'), 'mot_de_passe' => sha1($this->request->getVar('motdepasse'))];
		var_dump($login);
		$id =  $this->utilisateur->insert($login);
	}
}

<?php
namespace App\ThirdParty;
use App\ThirdParty\FPDF;

class PDF extends FPDF
{
	var $widths;
	var $aligns;

	function Header()
	{
		$this->SetFont('Arial', 'B', 12);
		// $this->Image('logo_epbl.jpg',10,3,30);
		$this->Cell(70);
		$this->SetY(0);
		$this->SetX(80);
		$this->Cell(25, 25, "       EGLISE DU CHRIST AU CONGO", 0, 0, 'C');
		$this->Ln(6);
		$this->SetX(80);
		$this->SetFont('Times', '', 12);
		$this->Cell(25, 25, "       UNION DES EGLISES BAPTISTES AU CONGO", 0, 0, 'C');
		$this->Ln(6);
		$this->SetX(80);
		$this->Cell(25, 25, "       ECC/UEBCO", 0, 0, 'C');
		$this->Ln(6);
		$this->SetX(80);
		$this->SetFont('Times', '', 16);
		$this->Cell(25, 25, "       COMPTABILITE", 0, 0, 'C');

		// personnalité juridique et autre ici
		$this->Ln(10);
		$this->SetX(100);
	}

	function Footer()
	{
		// // Positionnement à 1,5 cm du bas
		// $this->SetY(-15);
		// // Police Arial italique 8
		// $this->SetFont('Arial','I',8);
		// // Numéro de page
		// $this->Cell(0,10,' Page '.$this->PageNo().'/{nb}',0,0,'C');
	}

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths = $w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns = $a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb = 0;
		for ($i = 0; $i < count($data); $i++)
			$nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h = 5 * $nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for ($i = 0; $i < count($data); $i++) {
			$w = $this->widths[$i];
			$a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x = $this->GetX();
			$y = $this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$this->MultiCell($w, 5, $data[$i], 0, $a);
			//Put the position to the right of the cell
			$this->SetXY($x + $w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if ($this->GetY() + $h > $this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w, $txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw = &$this->CurrentFont['cw'];
		if ($w == 0)
			$w = $this->w - $this->rMargin - $this->x;
		$wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
		$s = str_replace("\r", '', $txt);
		$nb = strlen($s);
		if ($nb > 0 and $s[$nb - 1] == "\n")
			$nb--;
		$sep = -1;
		$i = 0;
		$j = 0;
		$l = 0;
		$nl = 1;
		while ($i < $nb) {
			$c = $s[$i];
			if ($c == "\n") {
				$i++;
				$sep = -1;
				$j = $i;
				$l = 0;
				$nl++;
				continue;
			}
			if ($c == ' ')
				$sep = $i;
			$l += $cw[$c];
			if ($l > $wmax) {
				if ($sep == -1) {
					if ($i == $j)
						$i++;
				} else
					$i = $sep + 1;
				$sep = -1;
				$j = $i;
				$l = 0;
				$nl++;
			} else
				$i++;
		}
		return $nl;
	}
}

<?php

namespace App\Models;

use CodeIgniter\Model;

class RecetteModel extends Model
{
    protected $table     = 'trecette'; //nom de la table
    protected $primaryKey = 'id_recette'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['montant', 'id_eglise', 'mois_reference', 'date_recette'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

<?php

namespace App\Models;

use CodeIgniter\Model;

class CategorieModel extends Model
{
    protected $table     = 'tcategorie'; //nom de la table
    protected $primaryKey = 'id_categorie'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['designation'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

<?php

namespace App\Models;

use CodeIgniter\Model;

class DepenseModel extends Model
{
    protected $table     = 'tdepense'; //nom de la table
    protected $primaryKey = 'id_depense'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['montant', 'motif', 'date_depense'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

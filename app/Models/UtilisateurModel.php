<?php

namespace App\Models;

use CodeIgniter\Model;

class UtilisateurModel extends Model
{
    protected $table     = 'tutilisateur'; //nom de la table
    protected $primaryKey = 'id_utilisateur'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['nom_utilisateur', 'mot_de_passe'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppresion';
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

<?php

namespace App\Models;

use CodeIgniter\Model;

class DistrictModel extends Model
{
    protected $table     = 'tdistrict'; //nom de la table
    protected $primaryKey = 'id_district'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['nom'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

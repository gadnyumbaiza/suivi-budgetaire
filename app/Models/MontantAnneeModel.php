<?php

namespace App\Models;

use CodeIgniter\Model;

class MontantAnneeModel extends Model
{
    protected $table     = 'tmontant_annee'; //nom de la table
    protected $primaryKey = 'id_montant_annee'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['montant', 'id_annee_compt', 'id_categ'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

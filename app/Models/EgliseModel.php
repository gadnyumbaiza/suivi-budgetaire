<?php

namespace App\Models;

use CodeIgniter\Model;

class EgliseModel extends Model
{
    protected $table     = 'teglise'; //nom de la table
    protected $primaryKey = 'id_eglise'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['nom', 'id_district', 'nom_pasteur', 'telephone', 'commune', 'quartier', 'avenue'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppresion';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

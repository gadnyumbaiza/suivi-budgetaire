<?php

namespace App\Models;

use CodeIgniter\Model;

class AnneeComptableModel extends Model
{
    protected $table     = 'tannee_comptable'; //nom de la table
    protected $primaryKey = 'id_annee_compt'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['annee'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

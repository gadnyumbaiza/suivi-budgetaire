<?php

namespace App\Models;

use CodeIgniter\Model;

class AssociationCatEgliseModel extends Model
{
    protected $table     = 'tassociation_cat_eglise'; //nom de la table
    protected $primaryKey = 'id_association'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_eglise', 'id_categorie', 'date_changement'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

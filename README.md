# Application de Suivi Budgétaire

L'application est réalisée à l'aide de PHP et du Framework [CodeIgniter4](https://github.com/codeigniter4/CodeIgniter4)

[[_TOC_]]

## Authentification

![alt text](public/assets/images/Illustrations/authentification.png "Interface de connexion")

# Espace Administrateur
## Accueil

![alt text](public/assets/images/Illustrations/accueil.png "Interface d'accueil")

## Eglise

![alt text](public/assets/images/Illustrations/liste des eglises.png "Liste des eglises")

![alt text](public/assets/images/Illustrations/Ajout eglise.png "Ajout eglise")

## Recette

![alt text](public/assets/images/Illustrations/recette.png "Listes des recettes")

![alt text](public/assets/images/Illustrations/Rapport receette.png "Rapport receette")

